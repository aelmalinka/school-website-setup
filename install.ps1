Param(
	[String]$DatabaseName = 'database',
	[String]$DatabaseDockerFile = 'database.docker',
	[String[]]$DatabaseConfigFiles = @('pages.sql', 'users.sql'),
	[String]$DatabaseMount = '/var/lib/postgres',
	[String]$DatabaseImage = 'aelmalinka/postgres',

	[String[]]$Servers = @('pages','users','permissions'),
	[String]$ServersDockerFile = 'servers.docker',
	[String]$ServersConfigFile = 'config.json',
	[String]$ServersMount = '/code',
	[String]$ServersPort = '8080',
	[String]$ServerImage = 'aelmalinka/'
)

function Create-Mount(
	[String]$Source,
	[String]$Destination
) {
	$ret = New-Object PSObject

	$ret | Add-Member -MemberType NoteProperty -Name Source -Value $Source
	$ret | Add-Member -MemberType NoteProperty -Name Destination -Value $Destination
	
	$ret
}

function Create-Port(
	[String]$HostPort,
	[String]$Container
) {
	$ret = New-Object PSObject
	
	$ret | Add-Member -MemberType NoteProperty -Name Host -Value $HostPort
	$ret | Add-Member -MemberType NoteProperty -Name Container -Value $Container
	
	$ret
}

function Get-IP(
	[String]$Name = $DatabaseName
) {
	docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $Name
}

function Create-Config(
	[String]$IP = (Get-Ip),
	[String]$UserName = 'website',
	[String]$Password = 'asdf'
) {
	$config = New-Object psobject
	$database = New-Object psobject

	$database | Add-Member -MemberType NoteProperty -Name host -Value $IP
	$database | Add-Member -MemberType NoteProperty -Name user -Value $UserName
	$database | Add-Member -MemberType NoteProperty -Name password -Value $Password
	
	$config | Add-Member -MemberType NoteProperty -Name database -Value $database
	
	ConvertTo-Json $config
}

function Create-Workdir(
	[String]$Base = (pwd)
) {
	$name = [System.IO.Path]::GetRandomFileName()
	$path = "${Base}\${name}"
	New-Item -Type Directory -Path $path
}

function Create-Image(
	[String]$File,
	[String]$Path
) {
	docker build -f $File $Path | %{
		if($_ -Match 'Successfully') {
			$_.Split()[2]
		}
	}
}

function Create-Volume(
	[String]$Name
) {
	docker volume create $Name
}

function Create-Volumes {
	echo "Create Volumes"
	Create-Volume "${DatabaseName}-vol"
	$Servers | %{
		Create-Volume "${_}-vol"
	}
}

function Run-Image(
	[String]$Image,
	[String]$Name,
	[PSCustomObject[]]$Mounts,
	[PSCustomObject[]]$Ports,
	[Switch]$Remove,
	[Switch]$Detach
) {
	$opts = {@()}.Invoke()
	if($Remove) {
		$opts.Add('--rm')
	}
	if($Detach) {
		$opts.Add('-d')
	}
	if($Name) {
		$opts.Add('--name')
		$opts.Add($Name)
	}
	if($Mounts) {
		$Mounts | %{
			$opts.Add('--mount')
			$opts.Add("source=$($_.Source),destination=$($_.Destination)")
		}
	}
	if($Ports) {
		$Ports | %{
			$opts.Add('-p')
			$opts.Add("$($_.Host):$($_.Container)")
		}
	}
	docker run @opts $Image
}

function Remove-Image(
	[String]$Image
) {
	docker rmi $Image
}

function Init-DB(
	[String]$Work = (Create-Workdir)
) {
	Copy-Item $DatabaseConfigFiles $Work
	echo "Creating DB Image"
	$Image = Create-Image "${DatabaseDockerFile}" "$Work"
	echo "Running DB Image"
	Run-Image $Image -Remove -Mounts @(Create-Mount "${DatabaseName}-vol" $DatabaseMount)
	echo "Removing DB Image"
	Remove-Image $Image
	Remove-Item -Recurse $Work
}

function Init-Servers(
	[String]$Work = (Create-Workdir)
) {
	$Mounts = {@()}.Invoke()
	$Servers | %{
		$Mounts.Add((Create-Mount "${_}-vol" "/${_}"))
	}
	echo "Creating Servers Config File ($(Get-Ip))"
	Create-Config | Out-File -FilePath "${Work}/${ServersConfigFile}" -Encoding ASCII
	echo "Creating Config Image"
	$Image = Create-Image "${ServersDockerFile}" "$Work"
	echo "Running Config Image"
	Run-Image $Image -Remove -Mounts $Mounts
	echo "Removing Config Image"
	Remove-Image $Image
	Remove-Item -Recurse $Work
}

function Start-DB {
	echo "Starting Database Image"
	Run-Image $DatabaseImage $DatabaseName @(Create-Mount "${DatabaseName}-vol" $DatabaseMount) -Detach
}

function Start-Server(
	[String]$Image,
	[Int]$Port
) {
	echo "Starting ${Image} Image"
	Run-Image "${ServerImage}${Image}" $Image @(Create-Mount "${Image}-vol" $ServersMount) @(Create-Port $Port $ServersPort) -Detach
}

function Stop-Server(
	[String]$Name
) {
	docker stop $Name
	docker logs $Name
	docker wait $Name
}

function Run-Servers {
	$port = 8080
	$Servers | %{
		Start-Server $_ $port
		$port = $port + 1
	}
}

function Stop-Servers {
	$Servers | %{ Stop-Server $_ }
}

function Start-Servers {
	$Servers | %{ docker start $_ }
}

Create-Volumes

Init-DB
Start-DB

Run-Servers
Stop-Servers
Init-Servers
Start-Servers

# SIG # Begin signature block
# MIIFiwYJKoZIhvcNAQcCoIIFfDCCBXgCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQULFGk+NXzgEoPKKQ4OY3PAbDI
# kBOgggMcMIIDGDCCAgCgAwIBAgIQImqNBe+a0KlNohxgZVnsfDANBgkqhkiG9w0B
# AQsFADAkMSIwIAYDVQQDDBlNaWNoYWVsIFRob21hcyBQb3dlcnNoZWxsMB4XDTE5
# MDgwNDE3MTQxM1oXDTIwMDgwNDE3MzQxM1owJDEiMCAGA1UEAwwZTWljaGFlbCBU
# aG9tYXMgUG93ZXJzaGVsbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
# ALgrRLjuhZTGnCGW/vN+lvn4YLcZwQrkkjzGmtLxTKTeasGfjnfIb4g0gYS38JJq
# 61WH6CCXX7KZMrMF9pczcVbFkarPotuXObYOA7PF+C9rA/r2EU5+k8R7FmftAeKu
# /3it1x1iq7mVkDMkFffd2jbfgBqgbC6ttz+RzEraM8snJSQ8lY+ItZKe3Igk8u/B
# OHKPo7ixbjaOzQ0alkWTyyFXQ5BILHG+RH/495atZp9s8TGWJ18SF1ONzW+AJGjp
# Hkji0etBpdjy5gRKUvj3+hyXUre7Dy8m2hVO3vQF41hRpm3PVzlASC9lVxrOLpiF
# NcXm95g1LG669IkwnBeZ1jECAwEAAaNGMEQwDgYDVR0PAQH/BAQDAgeAMBMGA1Ud
# JQQMMAoGCCsGAQUFBwMDMB0GA1UdDgQWBBSlhbuuL5SjVZVjXLweFx0xBtjCoTAN
# BgkqhkiG9w0BAQsFAAOCAQEAgaBmi5dQSekt5B10K/q7asXp2iMsqlBsLZy5I6UX
# zjlkAJfQqUB7PkowOzvQxRjKDs4gsF+ZabcK10gPodzZr1+ki8yDeWTzcIDw5twC
# +W478zWf00uyozsxaTjsUecHVzQYHUX+irVymnuA1GRTgMz0DJW+Lmt3/2iyBO7H
# RJnGjPhEMq3wrQMJ3clYy86I7YXxrxNRfb4j2ufvhUK37NoKxYdrqE9MYsz6Uk0k
# m8JofUxqGMGh6FxUwbJByY32YV3xu/puB89ueJkS6LO85dlWIIUv4m1/8mytP0sN
# +5c9XsEaj1ZoiVKwiRZwRGrQo/TKm5dp1rM31mpXd9UnZjGCAdkwggHVAgEBMDgw
# JDEiMCAGA1UEAwwZTWljaGFlbCBUaG9tYXMgUG93ZXJzaGVsbAIQImqNBe+a0KlN
# ohxgZVnsfDAJBgUrDgMCGgUAoHgwGAYKKwYBBAGCNwIBDDEKMAigAoAAoQKAADAZ
# BgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgorBgEEAYI3AgELMQ4wDAYKKwYB
# BAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUhU/qK+bG6HiOI5pfH//pLyfVY9cwDQYJ
# KoZIhvcNAQEBBQAEggEAIsb0c9/AI+894utFUQkBwfVZRETXbxATebxnl35hyivR
# o07/L1fHOAJysBuf6VmV+yz3S3GrUTLsrdKAjMr+DXYb9lHBpnEcm+L14eNeTwrM
# H5hP356Wq90V1bJChG1Mor/HQYvwXB0V7zFki1+cwZdmDa+AB5LH7gN6qhJHgJtB
# g1c4vcjKwMPUhcEgwoxHOJHKUeU8wl59+DdH9S3Nq4Cr3f40FAYbQ57Pcs0oFLfl
# zC8fTWQpdIWrnDfQnDrFVYre1pMixey9FfXFM6GKFXR1ulnbnkfUSumATvb0lIjf
# I9LLhoqNJXfvw9JdScnKV4H4N/8xhdNXs22tp0HLkg==
# SIG # End signature block
