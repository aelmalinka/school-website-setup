-- Schema users

DROP SCHEMA IF EXISTS users CASCADE;
CREATE SCHEMA users;

-- Tables

CREATE TABLE users.users (
    name text NOT NULL PRIMARY KEY,
    hash text
);

CREATE TABLE users.permissions (
    id serial NOT NULL PRIMARY KEY,
    app text NOT NULL,
    data jsonb NOT NULL
);

CREATE TABLE users.users_x_permissions (
    "user" text REFERENCES users.users (name),
    permission integer NOT NULL REFERENCES users.permissions (id)
);

CREATE TABLE users.sessions_real (
    name text NOT NULL REFERENCES users.users (name),
    start timestamp DEFAULT now(),
    lmod timestamp DEFAULT now()
);

-- Views

CREATE VIEW users.permission AS
 SELECT tab.name,
    json_object_agg(tab.app, tab.perm) AS permissions
   FROM ( SELECT users.name,
            perms.app,
            json_object_agg(perms.key, perms.value) AS perm
           FROM ((users.users
             JOIN users.users_x_permissions ON ((users.name = users_x_permissions."user")))
             JOIN ( SELECT permissions.id,
                    permissions.app,
                    permissions.data,
                    jsonb_each.key,
                    jsonb_each.value
                   FROM users.permissions,
                    LATERAL jsonb_each(permissions.data) jsonb_each(key, value)) perms ON ((perms.id = users_x_permissions.permission)))
          GROUP BY users.name, perms.app) tab
  GROUP BY tab.name;

CREATE VIEW users.sessions AS
 SELECT sessions_real.name,
    encode(public.digest((sessions_real.name || sessions_real.start), 'sha256'::text), 'base64'::text) AS key,
    sessions_real.lmod
   FROM users.sessions_real
  WHERE (sessions_real.lmod > (now() - '10m'::interval));
  
-- Functions

CREATE FUNCTION users."create"(name text, pass text) RETURNS text LANGUAGE sql AS $$
	INSERT INTO users.users(name, hash) SELECT name, crypt(pass, gen_salt('md5')) RETURNING name
$$;

CREATE FUNCTION users.update_lmod(which text) RETURNS void LANGUAGE sql AS $$
	UPDATE users.sessions SET lmod = now() WHERE key = which;
$$;

CREATE FUNCTION users.update_user(oldName text, newName text, newPass text) RETURNS text LANGUAGE sql AS $$
	DELETE FROM users.users_x_permissions WHERE "user" = oldName;
	UPDATE users.users SET
		name = newName, hash = crypt(newPass, gen_salt('md5'))
		WHERE name = oldName
		RETURNING name
	;
$$;

-- Data

INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*1, */'pages', '{"view": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*2, */'pages', '{"view": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*3, */'pages', '{"list": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*4, */'pages', '{"list": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*5, */'pages', '{"add": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*6, */'pages', '{"add": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*7, */'pages', '{"edit": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*8, */'pages', '{"edit": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*9, */'pages', '{"remove": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*10, */'pages', '{"remove": false}');

INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*11, */'users', '{"list": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*12, */'users', '{"list": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*13, */'users', '{"view": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*14, */'users', '{"view": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*15, */'users', '{"add": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*16, */'users', '{"add": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*17, */'users', '{"edit": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*18, */'users', '{"edit": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*19, */'users', '{"remove": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*20, */'users', '{"remove": false}');

INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*21, */'permissions', '{"all": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*22, */'permissions', '{"all": false}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*23, */'permissions', '{"default": true}');
INSERT INTO users.permissions (/*id, */app, "data") VALUES (/*24, */'permissions', '{"default": false}');

INSERT INTO users.users VALUES ('default', NULL);
INSERT INTO users.users VALUES ('admin', '$1$/SROutH0$duCHJ1.LhVXnuRgx1Wtvz0');

INSERT INTO users.users_x_permissions VALUES ('default', 1);
INSERT INTO users.users_x_permissions VALUES ('default', 3);
INSERT INTO users.users_x_permissions VALUES ('default', 6);
INSERT INTO users.users_x_permissions VALUES ('default', 8);
INSERT INTO users.users_x_permissions VALUES ('default', 10);
INSERT INTO users.users_x_permissions VALUES ('default', 12);
INSERT INTO users.users_x_permissions VALUES ('default', 14);
INSERT INTO users.users_x_permissions VALUES ('default', 16);
INSERT INTO users.users_x_permissions VALUES ('default', 18);
INSERT INTO users.users_x_permissions VALUES ('default', 20);
INSERT INTO users.users_x_permissions VALUES ('default', 21);
INSERT INTO users.users_x_permissions VALUES ('default', 23);

INSERT INTO users.users_x_permissions VALUES ('admin', 5);
INSERT INTO users.users_x_permissions VALUES ('admin', 7);
INSERT INTO users.users_x_permissions VALUES ('admin', 9);
INSERT INTO users.users_x_permissions VALUES ('admin', 11);
INSERT INTO users.users_x_permissions VALUES ('admin', 13);
INSERT INTO users.users_x_permissions VALUES ('admin', 15);
INSERT INTO users.users_x_permissions VALUES ('admin', 17);
INSERT INTO users.users_x_permissions VALUES ('admin', 19);