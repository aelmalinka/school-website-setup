-- Schema

DROP SCHEMA IF EXISTS pages CASCADE;
CREATE SCHEMA pages;

-- Tables

CREATE TABLE pages."default" (
    name text NOT NULL PRIMARY KEY,
    body text NOT NULL
);

-- Filler Data

INSERT INTO pages."default" VALUES ('Home', 'This is a website');
INSERT INTO pages."default" VALUES ('ToDo', '# Android
## MainActivity
 - [ ] line 27: Clean this up
 - [ ] line 29: Refresh pages
## Page
 - [ ] line 9: runtime select page source?
 - [ ] line 36: Switch on error type
## Pages
 - [ ] line 8: runtime selectable?
# Backend
## Pages
 - [x] GET
 - [ ] PUT
 - [ ] POST
 - [ ] DELETE');
INSERT INTO pages."default" VALUES ('Page', '# Listy
1. Thingy?
2. Other Thingy
    a. asdf
    b. jkl
    c. listy
3. Mer');
